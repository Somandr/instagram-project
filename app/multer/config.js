const multer = require('multer');

const storage = multer.memoryStorage();
const multerUploads = multer({
    storage,
    // limits: {
    //     fileSize: 15 * 1024 * 1024,
    // },
});

module.exports = multerUploads;
