const mongoose = require('mongoose');
const config = require('config');

const mongoDb = async () => {
    await mongoose.connect(config.get('mongoUri'), {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
    });
    // eslint-disable-next-line no-console
    console.log('MongoDb ==> Connection successfully established!!!');
};

module.exports = mongoDb;
