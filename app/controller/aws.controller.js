const s3 = require('../s3/config');
const sharp = require('sharp');
const crypto = require('crypto');
const Post = require('../models/Post');

let arr = [];

exports.doUpload = (req, res) => {
    // eslint-disable-next-line prefer-destructuring
    const s3Client = s3.s3Client;
    const params = s3.uploadParams;

    const paramResize = [
        {
            size: { width: 1000 },
            format: 'webp',
        },
        {
            size: { width: 1000 },
            format: 'jpeg',
        },
    ];

    const image = sharp(req.file.buffer);
    paramResize.map((param) => {
        let hex;
        crypto.randomBytes(5, (err, buf) => {
            if (err) {
                // eslint-disable-next-line no-console
                return console.log(err);
            }
            hex = `${buf.toString('hex')}_${req.file.fieldname}`;
        });

        return image
            .toFormat(param.format)
            .resize(param.size)
            .withMetadata()
            .toBuffer({
                resolveWithObject: true,
            })
            .then((data) => {
                const file = data.data;
                const { width, height, format } = data.info;

                params.Key = `${hex}_${width}x${height}.${format}`;
                params.Body = file;

                s3Client.upload(params, (err, dataNew) => {
                    arr.push(dataNew.Location);
                });
            });
    });

    setTimeout(() => {
        const post = new Post({
            photos: arr,
            body: req.body.text,
            postedBy: req.body.user,
            filename: req.file.filename,
        });
        post.save()
            .then((result) => {
                res.json({ post: result });
            })
            .catch((err) => {
                // eslint-disable-next-line no-console
                console.log(err);
            });
    }, 4000);
    arr = [];
};
