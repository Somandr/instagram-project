import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Auth from './pages/Authorize/Auth';
import Login from './pages/Authorize/Login';
import Registration from './pages/Authorize/Registration';
import Profile from './containers/Profile/Profile';
import PostWrapper from './pages/Main/Main';
import Password from './pages/Authorize/Password';
import PasswdRecover from './pages/Authorize/PasswdRecover';

export const useRoutes = (isAuthenticated) => {
    if (isAuthenticated) {
        return (
            <Switch>
                <Route exact path="/posts">
                    <PostWrapper />
                </Route>
                <Route exact path="/profile/user">
                    <Profile />
                </Route>
                <Route path="/profile/:userid">
                    <Profile />
                </Route>
                <Redirect to="/posts" />
            </Switch>
        );
    }
    return (
        <Switch>
            <Route path="/" exact>
                <Auth />
            </Route>
            <Route path="/login" exact>
                <Login />
            </Route>
            <Route path="/signup" exact>
                <Registration />
            </Route>
            <Route exact path="/password">
                <Password />
            </Route>
            <Route path="/password/:token">
                <PasswdRecover />
            </Route>
            <Redirect to="/" />
        </Switch>
    );
};
