import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 500,
    },
    rootTab: {
        padding: '0',
    },
    indicator: {
        top: '0',
        height: '2px',
        position: 'absolute',
        transition: 'all 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    },
}));

export default function AccountPhotoWrapper() {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                aria-label="full width tabs example"
                className="my-tabs"
                classes={{
                    root: classes.rootTabs,
                    indicator: classes.indicator,
                }}
            >
                <Tab
                    label="публикации"
                    {...a11yProps(0)}
                    className="my-tab"
                    classes={{ root: classes.rootTab }}
                ></Tab>
                <Tab
                    label="отметки"
                    {...a11yProps(1)}
                    className="my-tab"
                    classes={{ root: classes.rootTab }}
                />
            </Tabs>
            <TabPanel value={value} index={0} dir={theme.direction}>
                Item one
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
                Item Two
            </TabPanel>
        </div>
    );
}
