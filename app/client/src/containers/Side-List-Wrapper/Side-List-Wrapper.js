import React from 'react';

import AvatarSideList from '../../components/Avatar/Avatar-side-list';
import RecomendedFriendList from '../Lists/Recomend-friend-list';

export default function SideListWrapper(props) {
    const { imgProfile, userUserName, userName, users, userId } = props;

    return (
        <>
            <AvatarSideList
                imgProfile={imgProfile}
                userUserName={userUserName}
                userName={userName}
            />
            <RecomendedFriendList users={users} userId={userId} />
        </>
    );
}
