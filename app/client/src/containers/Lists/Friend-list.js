import React from 'react';
import PropTypes from 'prop-types';
import { FixedSizeList } from 'react-window';

import Box from '@material-ui/core/Box';
import FriendListStyles from '../../assets/styles/FriendListStyles';
import AvatarSideFriendList from '../../components/Avatar/Avatar-side-friend-list';

function renderRow(props) {
    const { index, style } = props;

    return (
        <Box component="div" button style={style} key={index}>
            <AvatarSideFriendList />
        </Box>
    );
}

renderRow.propTypes = {
    index: PropTypes.number.isRequired,
    style: PropTypes.object.isRequired,
};

export default function FriendList() {
    const classes = FriendListStyles();

    return (
        <div className={classes.root}>
            <Box component="div">
                <Box component="p" className={classes.subHeader}>
                    Your friends
                </Box>
            </Box>
            <FixedSizeList height={150} width={300} itemSize={45} itemCount={6}>
                {renderRow}
            </FixedSizeList>
        </div>
    );
}
