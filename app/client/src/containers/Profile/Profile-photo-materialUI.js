import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import AppsIcon from '@material-ui/icons/Apps';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && <Box>{children}</Box>}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: '#11ffee00',
        width: 975,
    },
    indicator: {
        backgroundColor: 'black',
        top: '0',
    },
    tabRoot: {
        padding: '0',
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    labelIcon: {
        paddingTop: '0',
        minHeight: '48px',
    },
    textColorInherit: {
        opacity: '0.5',
    },
    '@media screen and (max-width: 910px)': {
        root: {
            justifyContent: 'center !Important',
            display: 'flex !Important',
        },
    },
}));

export default function PhotoTabs(props) {
    const { renderPosts } = props;

    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <Tabs
                value={value}
                onChange={handleChange}
                centered={true}
                classes={{ indicator: classes.indicator }}
            >
                <Tab
                    label="Публикации"
                    classes={{
                        wrapper: classes.wrapper,
                        labelIcon: classes.labelIcon,
                        root: classes.tabRoot,
                        textColorInherit: classes.textColorInherit,
                    }}
                    fontSize="small"
                    icon={<AppsIcon fontSize="small" />}
                    {...a11yProps(0)}
                />
                <Tab
                    label="Сохраненное"
                    classes={{
                        wrapper: classes.wrapper,
                        labelIcon: classes.labelIcon,
                        root: classes.tabRoot,
                        textColorInherit: classes.textColorInherit,
                    }}
                    fontSize="small"
                    icon={<BookmarkBorderIcon fontSize="small" />}
                    {...a11yProps(1)}
                />
            </Tabs>

            <TabPanel value={value} index={0} dir={theme.direction}>
                {renderPosts}
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
                Нет публикаций
            </TabPanel>
        </div>
    );
}
