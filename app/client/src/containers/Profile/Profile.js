import React, { useState, useContext, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';
import { AuthContext } from '../../context/AuthContext';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
} from '@material-ui/core';
import Slide from '@material-ui/core/Slide';
import AvatarAccountPage from '../../components/Avatar/Avatar-account-page';
import AccountPublications from '../../containers/Account-photo/Account-publications';
import ImageUpload from '../../components/CustomUpload/ImageUpload';

import { Loader } from '../../components/Loader/Loader';

import PhotoTabs from './Profile-photo-materialUI';
import ProfileStyle from '../../assets/styles/ProfileStyle';
import { useHttp } from '../../hooks/http.hook';
import { usePost } from '../../hooks/post.hook';

import ModalSettings from '../../components/Modals/ModalSettings';
import ModalFollowers from '../../components/Modals/ModalFollowers';
import ModalFollowing from '../../components/Modals/ModalFollowing';

const Transition = React.forwardRef((props, ref) => (
    <Slide direction="down" {...props} ref={ref} />
));

const Profile = () => {
    const { userId, token, logout } = useContext(AuthContext);
    const { setUserInfo } = useContext(UserContext);
    const { request } = useHttp();
    const {
        likePost,
        unlikePost,
        followUser,
        unfollowUser,
        makeComment,
        ISOtoLongDate,
        newPosts,
        newUserData,
    } = usePost();
    const { userid } = useParams();
    const [image, setImage] = useState('');
    const [userPhotos, setUserPhotos] = useState(null);
    const [open, setOpen] = useState(false);
    const [userData, setUserData] = useState('');
    const [userPosts, setUserPosts] = useState([]);
    const [modalSettings, setModalSettings] = useState(false);
    const [modalFollow, setModalFollow] = useState(false);
    const [modalFollowing, setModalFollowing] = useState(false);
    const [followersData, setFollowersData] = useState([]);
    const [followingData, setFollowingData] = useState([]);
    const [showFollow, setShowFollow] = useState(true);

    const classes = ProfileStyle();
    let userPhotoJpeg;
    useEffect(() => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    }, []);

    const fetchData = useCallback(async () => {
        try {
            const requestHandler = userid ? `user/${userid}` : 'user';
            const fetched = await request(
                `/api/authorized/user/profile/${requestHandler}`,
                'GET',
                null,
                {
                    Authorization: `Bearer ${token}`,
                }
            );
            setUserInfo(fetched.userInfo);
            setUserData(fetched.user);
            setUserPhotos(fetched.user.photos);
            setUserPosts(fetched.posts);
            setFollowersData(fetched.userFollowers.followers);
            setFollowingData(fetched.userFollowers.following);
            setShowFollow(!fetched.user.followers.includes(userId));
        } catch (e) {}
    }, [token, request, userId, setUserInfo, userid]);

    useEffect(() => {
        fetchData();
        if (newPosts.length) {
            setUserPosts(newPosts);
        }
        if (newUserData) {
            setUserData(newUserData);
        }
    }, [fetchData, newPosts, newUserData]);

    const modalSettingsHandler = () => {
        setModalSettings(!modalSettings);
    };

    const modalFollowersHandler = () => {
        setModalFollow(!modalFollow);
    };

    const modalFollowingHandler = () => {
        setModalFollowing(!modalFollowing);
    };

    const PostData = (e) => {
        e.preventDefault();
        const file = image;
        const formData = new FormData();
        formData.append('img', file);
        formData.append('userId', userId);
        fetch('/api/authorized/user/profile/photo', {
            method: 'PUT',
            body: formData,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setUserPhotos(data.photos);
                setUserData(data);
                setUserInfo(data);
                alert('Фото профиля добавлено, успешно!');
                handleClose();
            });
    };

    const likePostHandler = (id) => {
        likePost(id, userPosts);
    };

    const unlikePostHandler = (id) => {
        unlikePost(id, userPosts);
    };

    const makeCommentHandler = (text, postId) => {
        makeComment(text, postId, userPosts);
    };

    const imageData = (imageValue) => {
        setImage(imageValue);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const followUserHandler = (e, userIdent = userid) => {
        followUser(userIdent);
        setShowFollow(false);
    };

    const unfollowUserHandler = (e, userIdent = userid) => {
        unfollowUser(userIdent);
        setShowFollow(true);
    };

    const renderFollow = showFollow ? (
        <Box
            component="a"
            href="#"
            className={classes.accountEditProfile}
            onClick={followUserHandler}
        >
            Подписаться
        </Box>
    ) : (
        <Box
            component="a"
            href="#"
            className={classes.accountEditProfile}
            onClick={unfollowUserHandler}
        >
            Отменить подписку
        </Box>
    );
    if (userPhotos) {
        userPhotoJpeg = userPhotos[0].includes('jpeg')
            ? userPhotos[0]
            : userPhotos[1];
    }

    const renderPosts = userPosts.map((post, index) => {
        const { updatedAt, photos, comments, likes, postedBy, _id } = post;

        const photoJpeg = photos[0].includes('jpeg') ? photos[0] : photos[1];

        const photoPostedByJpeg = postedBy.photos[0].includes('jpeg')
            ? postedBy.photos[0]
            : postedBy.photos[1];

        const toggleLikes = !likes.includes(userId)
            ? likePostHandler
            : unlikePostHandler;
        const ruPostDate = ISOtoLongDate(updatedAt, 'ru-RU');

        return (
            <AccountPublications
                postImage={photoJpeg}
                profilePhoto={photoPostedByJpeg}
                key={index}
                postComments={comments.length}
                postLikes={likes.length}
                postCommentItems={comments}
                userName={postedBy.username}
                toggleLikes={toggleLikes}
                favoritesPost={likes.includes(userId)}
                postDate={ruPostDate}
                makeComment={makeCommentHandler}
                postId={_id}
            />
        );
    });

    return (
        <>
            {userPosts ? (
                <Box component="section">
                    <Box component="main" className={classes.main}>
                        <Box
                            component="section"
                            className={classes.mainContainer}
                        >
                            <AvatarAccountPage
                                handleClickOpen={handleClickOpen}
                                image={userPhotoJpeg}
                            />
                            <Box
                                component="section"
                                className={classes.accountInfoWrapper}
                            >
                                <Box
                                    component="div"
                                    className={
                                        classes.accountInfoNameSettingWrapper
                                    }
                                >
                                    <Box
                                        component="h2"
                                        className={classes.accountNickName}
                                    >
                                        {userData.username}
                                    </Box>

                                    {userId === userData._id ? (
                                        <Box
                                            component="a"
                                            href="#"
                                            className={
                                                classes.accountEditProfile
                                            }
                                        >
                                            Редактировать профиль
                                        </Box>
                                    ) : (
                                        renderFollow
                                    )}

                                    <IconButton
                                        aria-label="setting"
                                        onClick={modalSettingsHandler}
                                    >
                                        <Box
                                            component="span"
                                            className={
                                                classes.accountSettingButton
                                            }
                                        />
                                    </IconButton>
                                </Box>
                                <Box
                                    component="ul"
                                    className={
                                        classes.accountInfoFollowersWrapper
                                    }
                                >
                                    <Box
                                        component="li"
                                        className={classes.accountPosts}
                                    >
                                        <Box
                                            component="a"
                                            href="#"
                                            className={
                                                classes.accountFollowersLinks
                                            }
                                        >
                                            <Box
                                                component="span"
                                                className={
                                                    classes.accountFollowersLinksSpan
                                                }
                                            >
                                                {userPosts.length}
                                            </Box>
                                            публикаций
                                        </Box>
                                    </Box>
                                    <Box
                                        component="li"
                                        className={classes.accountFollowers}
                                        onClick={modalFollowersHandler}
                                    >
                                        <Box
                                            component="a"
                                            href="#"
                                            className={
                                                classes.accountFollowersLinks
                                            }
                                        >
                                            <Box
                                                component="span"
                                                className={
                                                    classes.accountFollowersLinksSpan
                                                }
                                            >
                                                {userData.followers
                                                    ? userData.followers.length
                                                    : 0}
                                            </Box>
                                            подписчиков
                                        </Box>
                                    </Box>
                                    <Box
                                        component="li"
                                        className={classes.accountFollowings}
                                        onClick={modalFollowingHandler}
                                    >
                                        <Box
                                            component="a"
                                            href="#"
                                            className={
                                                classes.accountFollowersLinks
                                            }
                                        >
                                            <Box
                                                component="span"
                                                className={
                                                    classes.accountFollowersLinksSpan
                                                }
                                            >
                                                {userData.following
                                                    ? userData.following.length
                                                    : 0}
                                            </Box>
                                            подписки
                                        </Box>
                                    </Box>
                                </Box>
                                <Box
                                    component="div"
                                    className={
                                        classes.accountInfoFullNameWrapper
                                    }
                                >
                                    <Box
                                        component="h1"
                                        className={classes.accountFullName}
                                    >
                                        {userData.name}
                                    </Box>
                                    <Box
                                        component="div"
                                        className={
                                            classes.accountYourFriendFollowersWrapper
                                        }
                                        onClick={modalFollowersHandler}
                                    >
                                        <Box
                                            component="p"
                                            className={
                                                classes.accountYourFriendFollowersText
                                            }
                                        >
                                            Подписаны:
                                        </Box>
                                        <Box
                                            component="a"
                                            href="#"
                                            className={
                                                classes.accountYourFriendFollowersItem
                                            }
                                        >
                                            {followersData.length
                                                ? followersData[0].username
                                                : null}
                                        </Box>
                                        <Box
                                            component="a"
                                            href="#"
                                            className={
                                                classes.accountYourFriendFollowersMoreItem
                                            }
                                        >
                                            {followersData.length
                                                ? 'и другие'
                                                : 'нет подписчиков'}
                                        </Box>
                                    </Box>
                                </Box>
                            </Box>
                        </Box>

                        <Box
                            component="div"
                            className={classes.toggleButtonsWrapper}
                        >
                            <PhotoTabs renderPosts={renderPosts} />
                        </Box>

                        <Dialog
                            className={classes.modal}
                            disableBackdropClick={false}
                            disableEscapeKeyDown={false}
                            open={open}
                            onClose={handleClose}
                            TransitionComponent={Transition}
                            fullWidth={true}
                            maxWidth="sm"
                        >
                            <DialogTitle>Изменить фото профиля</DialogTitle>
                            <DialogContent>
                                <h4>Выберите фото</h4>
                                <Box className={classes.dialogContentWrap}>
                                    <ImageUpload
                                        addButtonProps={{ round: true }}
                                        changeButtonProps={{ round: true }}
                                        removeButtonProps={{
                                            round: true,
                                            color: 'info',
                                        }}
                                        imageData={imageData}
                                    />
                                </Box>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary">
                                    Отмена
                                </Button>
                                <Button onClick={PostData} color="primary">
                                    Опубликовать
                                </Button>
                            </DialogActions>
                        </Dialog>
                        <ModalSettings
                            openModal={modalSettings}
                            modalToggleHandler={modalSettingsHandler}
                            logout={logout}
                        />
                        <ModalFollowers
                            openModal={modalFollow}
                            modalToggleHandler={modalFollowersHandler}
                            followUserHandler={followUserHandler}
                            followersData={
                                followersData === undefined ? [] : followersData
                            }
                            userId={userId}
                        />
                        <ModalFollowing
                            openModal={modalFollowing}
                            modalToggleHandler={modalFollowingHandler}
                            followingData={
                                followingData === undefined ? [] : followingData
                            }
                            followUserHandler={followUserHandler}
                            userId={userId}
                        />
                    </Box>
                </Box>
            ) : (
                <Loader />
            )}
        </>
    );
};

export default Profile;
