import React, { useState, useMemo } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { useRoutes } from './appRoutes';
import { useTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core';
import { useAuth } from './hooks/auth.hook';
import { AuthContext } from './context/AuthContext';
import NavBar from './components/NavBar/NavBar';
import { Loader } from './components/Loader/Loader';
import Footer from './components/Footer/Footer';
import { UserContext } from './context/UserContext';

const App = () => {
    const [userInfo, setUserInfo] = useState(null);

    const { token, login, logout, userId, ready } = useAuth();

    const isAuthenticated = !!token;
    const routes = useRoutes(isAuthenticated);
    const theme = useTheme();

    const value = useMemo(() => ({ userInfo, setUserInfo }), [
        userInfo,
        setUserInfo,
    ]);

    if (!ready) {
        return <Loader />;
    }

    return (
        <AuthContext.Provider
            value={{
                token,
                login,
                logout,
                userId,
                isAuthenticated,
            }}
        >
            <UserContext.Provider value={value}>
                <Router>
                    {isAuthenticated && <NavBar />}
                    <ThemeProvider theme={theme}>{routes}</ThemeProvider>
                    <Footer />
                </Router>
            </UserContext.Provider>
        </AuthContext.Provider>
    );
};

export default App;
