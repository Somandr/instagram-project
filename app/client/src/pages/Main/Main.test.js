import React from 'react';
import Main from './Main';
import { act } from 'react-dom/test-utils';
import { render, unmountComponentAtNode } from 'react-dom';

let container = null;
beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

describe('Testing main', () => {
    test('Main are rendered successfully', () => {
        act(() => {
            render(<Main />, container);
        });
    });
});
