import React, { useContext, useState, useEffect, useCallback } from 'react';
import { NavLink } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { UserContext } from '../../context/UserContext';
import { useInfiniteScroll } from '../../hooks/infScroll.hook';
import { useHttp } from '../../hooks/http.hook';
import { usePost } from '../../hooks/post.hook';

import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import { Avatar } from '@material-ui/core';

import MainStyles from '../../assets/styles/MainStyles';
import PostCard from '../../components/Cards/PostCard';
import SideListWrapper from '../../containers/Side-List-Wrapper/Side-List-Wrapper';
import UsersCarousel from '../../components/UsersCarousel/UsersCarousel';

function Main() {
    const { userInfo, setUserInfo } = useContext(UserContext);
    const { userId, token } = useContext(AuthContext);
    const { request } = useHttp();
    const [isFetching, setIsFetching] = useInfiniteScroll(fetchMoreListItems);
    const [userPosts, setUserPosts] = useState([]);
    const [users, setUsers] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(3);
    const [loadPosts, setLoadPosts] = useState([]);

    const {
        likePost,
        unlikePost,
        makeComment,
        deletePost,
        ISOtoLongDate,
        newPosts,
    } = usePost();

    const classes = MainStyles();
    let userInfoPhotos;
    let username;
    let name;

    useEffect(() => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    }, []);

    const fetchData = useCallback(async () => {
        try {
            const fetched = await request(
                '/api/authorized/post/getsubpost',
                'GET',
                null,
                {
                    Authorization: `Bearer ${token}`,
                }
            );
            setUserPosts(fetched.mypost);
            setUserInfo(fetched.user);
            setUsers(fetched.users);
        } catch (e) {}
    }, [token, request, setUserInfo]);

    useEffect(() => {
        setLoadPosts(newPosts);
    }, [newPosts]);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    function fetchMoreListItems() {
        setCurrentPage((prevState) => prevState + 1);
        const indexOfLastPost = (currentPage + 1) * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        const currentPosts = userPosts.slice(indexOfFirstPost, indexOfLastPost);
        setLoadPosts((prevState) => [...prevState, ...currentPosts]);
        setIsFetching(false);
    }

    useEffect(() => {
        const indexOfLastPost = currentPage * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        setLoadPosts(userPosts.slice(indexOfFirstPost, indexOfLastPost));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userPosts]);

    const likePostHandler = (id) => {
        likePost(id, loadPosts);
    };

    const unlikePostHandler = (id) => {
        unlikePost(id, loadPosts);
    };

    const makeCommentHandler = (text, postId) => {
        makeComment(text, postId, loadPosts);
    };

    const deletePostHandler = (postId) => {
        deletePost(postId, loadPosts);
    };

    if (userInfo) {
        userInfoPhotos = userInfo.photos[0].includes('jpeg')
            ? userInfo.photos[0]
            : userInfo.photos[1];
        username = userInfo.username;
        name = userInfo.name;
    }

    const renderUsers = users.map((userProfile, index) => {
        const pathToProfileStories =
            userProfile._id !== userId
                ? `/profile/${userProfile._id}`
                : '/profile/user';

        const photoJpeg = userProfile.photos[0].includes('jpeg')
            ? userProfile.photos[0]
            : userProfile.photos[1];

        return (
            <Box key={index} className={classes.wrapStories}>
                <NavLink to={pathToProfileStories} key={index}>
                    <Box className={classes.imgLogoStories} />
                    <Avatar
                        aria-label="recipe"
                        alt="Profile Photo"
                        src={photoJpeg}
                        className={classes.avatar}
                    />
                </NavLink>

                <NavLink
                    to={pathToProfileStories}
                    className={classes.textLogoStories}
                >
                    <span>{userProfile.username}</span>
                </NavLink>
            </Box>
        );
    });
    const renderUserPosts = loadPosts.map((post, index) => {
        const postCardController = {
            pathToProfile:
                post.postedBy._id !== userId
                    ? `/profile/${post.postedBy._id}`
                    : '/profile/user',
            cardAvatarCheck: post.postedBy.photos[0].includes('jpeg')
                ? post.postedBy.photos[0]
                : post.postedBy.photos[1],
            cardImageCheck: post.photos[0].includes('jpeg')
                ? post.photos[0]
                : post.photos[1],
            toggleLikes: !post.likes.includes(userId)
                ? likePostHandler
                : unlikePostHandler,
            favoritesPost: post.likes.includes(userId),
            ruPostDate: ISOtoLongDate(post.createdAt, 'ru-RU'),
        };

        const postCardApi = {
            pathToProfile: postCardController.pathToProfile,
            userPostedById: post.postedBy._id,
            cardTitle: post.postedBy.username,
            cardAvatar: postCardController.cardAvatarCheck,
            cardImage: postCardController.cardImageCheck,
            postId: post._id,
            userId,
            postLikes: post.likes.length,
            postDate: postCardController.ruPostDate,
            postComments: post.comments,
            favoritesPost: postCardController.favoritesPost,
            toggleLikes: postCardController.toggleLikes,
            makeCommentHandler,
            deletePostHandler,
        };
        return <PostCard key={index} postCardApi={postCardApi} />;
    });

    return (
        <Box component="section">
            <Box component="main" className={classes.main}>
                <Box component="section" className={classes.mainContainer}>
                    <Box component="div" className={classes.postCard}>
                        <Box className={classes.usersStories}>
                            <UsersCarousel renderUsers={renderUsers} />
                        </Box>
                        {renderUserPosts}
                        {isFetching && (
                            <Box className={classes.postLoaderWrap}>
                                <CircularProgress
                                    disableShrink
                                    className={classes.postLoader}
                                />
                            </Box>
                        )}
                    </Box>
                    <Box component="div" className={classes.sideListWrapper}>
                        <SideListWrapper
                            imgProfile={userInfoPhotos}
                            userUserName={username}
                            userName={name}
                            users={users}
                            userId={userId}
                        />
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

export default Main;
