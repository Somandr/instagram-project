import React, { useContext, useState, useEffect } from 'react';
import { useHttp } from '../../hooks/http.hook';
import { AuthContext } from '../../context/AuthContext';

import AuthStyles from '../../assets/styles/AuthStyles';
import { SignIn } from '../../components/Auth/SignIn';
import { Loader } from '../../components/Loader/Loader';
import { Grid, Box } from '@material-ui/core';
import Message from '../../components/Message/Message';

const Login = () => {
    const auth = useContext(AuthContext);
    const { loading, request, error, clearError } = useHttp();
    const [openPopover, setOpenPopover] = useState(false);
    const [textError, setTextError] = useState('');
    const classes = AuthStyles();

    useEffect(() => {
        if (error) {
            setTextError(error);
            setOpenPopover(true);
            setTimeout(() => {
                setOpenPopover(false);
            }, 6000);
            console.log(error);
        }
        clearError();
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    }, [error, clearError]);

    const closePopoverHandler = () => {
        setOpenPopover(false);
    };

    if (loading) {
        return <Loader />;
    }

    const loginHandler = async (formData) => {
        try {
            const data = await request('/api/authorize/login', 'POST', {
                ...formData,
            });
            auth.login(data.token, data.userId);
        } catch (e) {}
    };

    return (
        <Box component="section" className={classes.signInSection}>
            <Grid
                container
                justify="center"
                alignItems="center"
                component="main"
                className={classes.signInContainer}
            >
                <SignIn loginHandler={loginHandler} />
                <Message
                    openPopover={openPopover}
                    textError={textError}
                    closePopoverHandler={closePopoverHandler}
                />
            </Grid>
        </Box>
    );
};

export default Login;
