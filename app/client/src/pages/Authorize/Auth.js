import React, { useContext, useState, useEffect } from 'react';

import { useHttp } from '../../hooks/http.hook';
import { AuthContext } from '../../context/AuthContext';

import { SignIn } from '../../components/Auth/SignIn';
import { Loader } from '../../components/Loader/Loader';
import { SliderAuth } from '../../components/Auth/Slider';
import Message from '../../components/Message/Message';

const Auth = () => {
    const auth = useContext(AuthContext);
    const { loading, request, error, clearError } = useHttp();
    const [openPopover, setOpenPopover] = useState(false);
    const [textError, setTextError] = useState('');

    useEffect(() => {
        if (error) {
            setTextError(error);
            setOpenPopover(true);
            setTimeout(() => {
                setOpenPopover(false);
            }, 6000);
            console.log(error);
        }
        clearError();
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    }, [error, clearError]);

    if (loading) {
        return <Loader />;
    }

    const closePopoverHandler = () => {
        setOpenPopover(false);
    };

    const loginHandler = async (formData) => {
        try {
            const data = await request('/api/authorize/login', 'POST', {
                ...formData,
            });
            auth.login(data.token, data.userId);
        } catch (e) {}
    };

    return (
        <SliderAuth>
            <SignIn loginHandler={loginHandler} />
            <Message
                openPopover={openPopover}
                textError={textError}
                closePopoverHandler={closePopoverHandler}
            />
        </SliderAuth>
    );
};

export default Auth;
