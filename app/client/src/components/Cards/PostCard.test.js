import React from 'react';
import PostCard from './PostCard';
import { act } from 'react-dom/test-utils';
import { render, unmountComponentAtNode } from 'react-dom';

// jest.mock('../../pages/Main/Main', () => {
//     return (props) => (
//         <div className="test-card">
//             {props}
//         </div>
//     )
// });

let container = null;
beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

describe('Testing PostCard', () => {
    test('PostCard are rendered successfully', () => {
        act(() => {
            render(<PostCard />, container);
        });
    });
});
