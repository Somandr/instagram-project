import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(() => ({
    root: {
        width: '100%',
        maxWidth: '36ch',
        backgroundColor: '#FAFAFA',
        paddingLeft: 20,
    },
    avatarMain: {
        color: '#262626',
        textDecoration: 'none',
    },
    avatarTitle: {
        padding: '6px 0 0 15px',
    },
    avatarSubtitle: {
        display: 'inline',
        color: '#8E8E8E',
        textTransform: 'capitalize',
        fontSize: '12px',
    },
    avaMain: {
        width: 56,
        height: 56,
    },
}));

export default function AvatarSideList(props) {
    const classes = useStyles();
    const { imgProfile, userUserName, userName } = props;

    return (
        <List className={classes.root}>
            <NavLink to="/profile/user" className={classes.avatarMain}>
                <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar
                            className={classes.avaMain}
                            alt="Remy Sharp"
                            src={imgProfile}
                        />
                    </ListItemAvatar>

                    <ListItemText
                        className={classes.avatarTitle}
                        primary={<>{userUserName}</>}
                        secondary={<>{userName}</>}
                    />
                </ListItem>
            </NavLink>
        </List>
    );
}
