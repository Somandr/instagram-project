import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import AvatarSideFriendListStyles from '../../assets/styles/AvatarSideFriendListStyles';

export default function AvatarSideFriendList() {
    const classes = AvatarSideFriendListStyles();

    return (
        <div className={classes.root}>
            <Box component="div" className={classes.avatarWrapper}>
                <Avatar className={classes.small} />
                <Box component="div">
                    <Typography
                        component="p"
                        variant="body2"
                        className={classes.avatarName}
                        color="textPrimary"
                    >
                        Petrovich
                    </Typography>
                </Box>
            </Box>
        </div>
    );
}
