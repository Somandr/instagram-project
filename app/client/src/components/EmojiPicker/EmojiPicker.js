import React, { useState } from 'react';

// import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';

const EmojiPicker = () => {
    const [emojiPickerState, SetEmojiPicker] = useState(false);
    const [message, SetMessage] = useState('');

    let emojiPicker;
    if (emojiPickerState) {
        emojiPicker = (
            <Picker
                title="Pick your emoji…"
                emoji="point_up"
                onSelect={(emoji) => SetMessage(message + emoji.native)}
            />
        );
    }

    function triggerPicker(event) {
        event.preventDefault();
        SetEmojiPicker(!emojiPickerState);
    }

    return (
        <div className="w-100 sans-serif bg-white">
            <form
                className="pa4 black-80"
                style={{ maxWidth: '500px', margin: '0 auto' }}
            >
                <div className="measure">
                    <label for="name" className="f6 b db mb2">
                        Your message for us
                    </label>
                    <input
                        id="name"
                        className="input-reset ba b--black-20 pa2 mb2 db w-100"
                        type="text"
                        aria-describedby="name-desc"
                        value={message}
                        onChange={(event) => SetMessage(event.target.value)}
                    />
                    {emojiPicker}
                    <button
                        type="button"
                        className="ma4 b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"
                        onClick={triggerPicker}
                    >
                        Add an Emoji!
                        <span role="img" aria-label="">
                            😁
                        </span>
                    </button>

                    <div className="measure">
                        <label for="name" className="f6 b db mb2">
                            Your Inserted text
                        </label>
                        <input
                            className="f4 fw7 dib pa2 no-underline ba b--black-20 w-100"
                            value={message}
                            disabled
                        />
                    </div>

                    <button
                        className="ma4 b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"
                        type="submit"
                    >
                        Send your message
                    </button>
                </div>
            </form>
        </div>
    );
};

export default EmojiPicker;
