import React from 'react';
import { Box, makeStyles } from '@material-ui/core';

const FooterStyles = makeStyles(() => ({
    footer: {
        width: '100%',
    },
    footerWrap: {
        width: '1015px',
        margin: '0 auto',
        paddingTop: '50px',
        '& a': {
            textDecoration: 'none',
        },
    },
    footerLinks: {
        display: 'flex',
        marginRight: '16px',
        justifyContent: 'center',
        textTransform: 'uppercase',
        color: '#233A69',
        fontSize: '12px',
        fontWeight: 'bold',
        '& a': {
            marginRight: '16px',
            color: '#233A69',
        },
        '& a:visited': {
            color: '#233A69',
        },
    },
    footerCopyRightWrap: {
        display: 'flex',
        justifyContent: 'start',
        color: '#8E8E8E',
        fontSize: '12px',
        textTransform: 'uppercase',
        fontWeight: 700,
        margin: '15px 0 38px 0',
    },
    '@media screen and (max-width: 910px)': {
        footerWrap: {
            display: 'none',
        },
    },
}));

const Footer = () => {
    const footerLinks = [
        'Информация',
        'Помощь',
        'Пресса',
        'API',
        'Вакансии',
        'Кофиденциальность',
        'Условия',
        'Места',
        'Популярные аккаунты',
        'Хештеги',
        'Язык',
    ];
    const renderLinks = footerLinks.map((link, index) => {
        return (
            <Box component="a" href="#" key={index}>
                {link}
            </Box>
        );
    });

    const classes = FooterStyles();
    return (
        <Box component="footer" className={classes.footer}>
            <Box className={classes.footerWrap}>
                <Box className={classes.footerLinks}>{renderLinks}</Box>
                <Box className={classes.footerCopyRightWrap}>
                    <Box component="span" className={classes.footerCopyright}>
                        &copy; Instagram от Facebook,{' '}
                        {new Date().toLocaleString('en', { year: 'numeric' })}
                    </Box>
                </Box>
            </Box>
        </Box>
    );
};

export default Footer;
