import React, { forwardRef } from 'react';
import {
    Dialog,
    Slide,
    Box,
    MenuList,
    Button,
    Avatar,
} from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const Transition = forwardRef((props, ref) => (
    <Slide direction="down" {...props} ref={ref} />
));

const ModalFollowers = (props) => {
    const {
        openModal,
        modalToggleHandler,
        followersData,
        userId,
        followUserHandler,
    } = props;

    const renderMenuLinks = followersData.map((item, index) => {
        const pathToProfile =
            item._id !== userId ? `/profile/${item._id}` : '/profile/user';

        const photoJpeg = item.photos[0].includes('jpeg')
            ? item.photos[0]
            : item.photos[1];

        const isUser = item._id === userId;

        return (
            <Box
                key={index}
                style={{ display: 'flex', justifyContent: 'space-between' }}
            >
                <NavLink
                    to={pathToProfile}
                    onClick={modalToggleHandler}
                    style={{
                        display: 'flex',
                        height: 40,
                        padding: '8px 14px',
                        textDecoration: 'none',
                    }}
                >
                    <Box>
                        <Avatar
                            aria-label="recipe"
                            alt="Profile Photo"
                            src={photoJpeg}
                            style={{
                                width: 38,
                                height: 38,
                                margin: '0px 8px 0 0',
                            }}
                        />
                    </Box>
                    <Box
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                        }}
                    >
                        <span
                            style={{
                                marginBottom: 0,
                                overflow: 'hidden',
                                fontWeight: 500,
                                textOverflow: 'ellipsis',
                                fontSize: 14,
                            }}
                        >
                            {item.username}
                        </span>
                        <span
                            style={{
                                fontSize: '14px',
                                fontWeight: 400,
                                overflow: 'hidden',
                                textAlign: 'left',
                                textOverflow: 'ellipsis',
                                color: '#8E8E8E',
                            }}
                        >
                            {item.name}
                        </span>
                    </Box>
                </NavLink>
                {item.followers.includes(userId) || isUser ? (
                    <Button
                        style={{
                            height: 30,
                            border: '1px solid #F0F0F0',
                            textTransform: 'none',
                            color: '#262626',
                            margin: '15px 19px',
                        }}
                    >
                        {isUser ? 'Ваш профиль' : 'Подписка'}
                    </Button>
                ) : (
                    <Button
                        onClick={() => followUserHandler(null, item._id)}
                        style={{
                            background: '#0095f6',
                            height: 30,
                            textTransform: 'none',
                            color: '#fff',
                            margin: '15px 19px',
                        }}
                    >
                        Подписаться
                    </Button>
                )}
            </Box>
        );
    });

    return (
        <Dialog
            disableBackdropClick={false}
            disableEscapeKeyDown={false}
            open={openModal}
            onClose={modalToggleHandler}
            TransitionComponent={Transition}
            maxWidth="sm"
            width={400}
            style={{ maxHeight: 400, overflow: 'auto', whiteSpace: 'nowrap' }}
        >
            <Box
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '100%',
                    minWidth: 400,
                    height: 42,
                    color: '#262626',
                    fontSize: 16,
                    fontWeight: 600,
                    borderBottom: '1px solid #F0F0F0',
                }}
            >
                <span
                    style={{
                        fontSize: 14,
                        color: '#262626',
                    }}
                >
                    Подписчики
                </span>
                <Box
                    style={{
                        float: 'right',
                        position: 'absolute',
                        right: 0,
                        top: 0,
                        padding: 8,
                        cursor: 'pointer',
                    }}
                    onClick={modalToggleHandler}
                >
                    <svg
                        aria-label="Закрыть"
                        fill="#262626"
                        height="24"
                        viewBox="0 0 48 48"
                        width="24"
                    >
                        <path d="M41.1 9.1l-15 15L41 39c.6.6.6 1.5 0 2.1s-1.5.6-2.1 0L24 26.1l-14.9 15c-.6.6-1.5.6-2.1 0-.6-.6-.6-1.5 0-2.1l14.9-15-15-15c-.6-.6-.6-1.5 0-2.1s1.5-.6 2.1 0l15 15 15-15c.6-.6 1.5-.6 2.1 0 .6.6.6 1.6 0 2.2z" />
                    </svg>
                </Box>
            </Box>
            <MenuList
                id="menu-list-grow"
                style={{
                    maxHeight: 364,
                    overflow: 'auto',
                    whiteSpace: 'nowrap',
                    padding: 0,
                }}
            >
                {renderMenuLinks}
            </MenuList>
        </Dialog>
    );
};

export default ModalFollowers;
