import React from 'react';
import { Grid, Box } from '@material-ui/core';
import { CarouselProvider, Slider, Slide } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import slide1 from '../../assets/img/signIn/img_signin_slide1.jpg';
import slide2 from '../../assets/img/signIn/img_signin_slide2.jpg';
import slide3 from '../../assets/img/signIn/img_signin_slide3.jpg';
import slide4 from '../../assets/img/signIn/img_signin_slide4.jpg';
import slide5 from '../../assets/img/signIn/img_signin_slide5.jpg';
import AuthStyles from '../../assets/styles/AuthStyles';

export const SliderAuth = (props) => {
    const classes = AuthStyles();
    return (
        <Box component="section" className={classes.signInSection}>
            <Grid
                container
                justify="center"
                alignItems="center"
                component="main"
                className={classes.signInContainer}
            >
                <Grid item className={classes.signInSliderWrapper}>
                    <Box className={classes.slideWrap}>
                        <Box className={classes.slider}>
                            <CarouselProvider
                                naturalSlideWidth={50}
                                naturalSlideHeight={125}
                                totalSlides={5}
                                isPlaying
                            >
                                <Slider>
                                    <Slide index={0}>
                                        <img
                                            className={classes.slideImage}
                                            src={slide1}
                                            alt="slide"
                                        />
                                    </Slide>
                                    <Slide index={1}>
                                        <img
                                            className={classes.slideImage}
                                            src={slide2}
                                            alt="slide"
                                        />
                                    </Slide>
                                    <Slide index={2}>
                                        <img
                                            className={classes.slideImage}
                                            src={slide3}
                                            alt="slide"
                                        />
                                    </Slide>
                                    <Slide index={3}>
                                        <img
                                            className={classes.slideImage}
                                            src={slide4}
                                            alt="slide"
                                        />
                                    </Slide>
                                    <Slide index={4}>
                                        <img
                                            className={classes.slideImage}
                                            src={slide5}
                                            alt="slide"
                                        />
                                    </Slide>
                                </Slider>
                            </CarouselProvider>
                        </Box>
                    </Box>
                </Grid>

                {props.children}
            </Grid>
        </Box>
    );
};
