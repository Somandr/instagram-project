import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Grid, Box } from '@material-ui/core';

import AuthStyles from '../../assets/styles/AuthStyles';
import Input from '../Input';
import ButtonMain from '../ButtonMain';

export const SignIn = (props) => {
    const [btn, setBtn] = useState(true);
    const [form, setForm] = useState({
        email: '',
        password: '',
    });

    const classes = AuthStyles();

    const { loginHandler } = props;

    const changeHandler = (event) => {
        if (event.target.value.length >= 6) {
            setBtn(false);
        } else {
            setBtn(true);
        }
        setForm({ ...form, [event.target.type]: event.target.value });
    };

    return (
        <Grid item className={classes.signInFormWrapper}>
            <Box component="form" className={classes.signInForm} method="post">
                <Box component="div" className={classes.instaLogoFormWrapper}>
                    <Box component="span" className={classes.instaLogoForm} />
                </Box>
                <Box className={classes.inputsWrapper}>
                    <Input
                        fullWidth
                        autoComplete="email"
                        type="email"
                        placeholder="Номер телефона, имя пользователя или..."
                        variant="filled"
                        onChange={changeHandler}
                        disableUnderline={true}
                        autoFocus={false}
                    />

                    <Input
                        fullWidth
                        autoComplete="email"
                        type="password"
                        placeholder="Пароль"
                        variant="filled"
                        onChange={changeHandler}
                        disableUnderline={true}
                        autoFocus={false}
                    />

                    <ButtonMain
                        onClick={() => loginHandler(form)}
                        disabled={btn}
                    >
                        Войти
                    </ButtonMain>
                    <Box component="span" className={classes.devider}>
                        или
                    </Box>
                    <Box
                        component="a"
                        href="#"
                        className={classes.fbIconWrapper}
                    >
                        <Box component="span" className={classes.fbIcon} />
                        <Box component="span" className={classes.enterFacebook}>
                            Войти через Facebook
                        </Box>
                    </Box>
                    <NavLink to="/password" className={classes.recoverPass}>
                        Забыли пароль?
                    </NavLink>
                </Box>
            </Box>
            <Box className={classes.signUp}>
                <Box component="span">
                    У вас ещё нет аккаунта?
                    <NavLink to="/signup">Зарегистрироваться</NavLink>
                </Box>
            </Box>
            <Box component="p" className={classes.loadAdd}>
                Установите приложение.
            </Box>
            <Box className={classes.adds}>
                <Box component="a" href="#" className={classes.appstoreAdd} />
                <Box component="a" href="#" className={classes.androidAdd} />
            </Box>
        </Grid>
    );
};
