import React from 'react';
import {
    CarouselProvider,
    Slider,
    Slide,
    ButtonBack,
    ButtonNext,
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { Box } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

const UsersCarousel = (props) => {
    const { renderUsers } = props;

    return (
        <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={25}
            totalSlides={3}
        >
            <Slider moveThreshold={0.5} style={{ outline: 'none' }}>
                <Slide index={0}>
                    <Box
                        style={{
                            display: 'flex',
                            width: '100%',
                            height: '100%',
                            alignItems: 'center',
                            paddingTop: 17.5,
                        }}
                    >
                        {renderUsers}
                    </Box>
                </Slide>
            </Slider>
            <ButtonBack
                style={{
                    border: 'none',
                    opacity: 0.4,
                    position: 'absolute',
                    top: 50,
                    outline: 'none',
                    backgroundColor: '#11ffee00',
                }}
            >
                <ArrowBackIosIcon />
            </ButtonBack>
            <ButtonNext
                style={{
                    border: 'none',
                    opacity: 0.4,
                    position: 'absolute',
                    top: 50,
                    right: 0,
                    outline: 'none',
                    backgroundColor: '#11ffee00',
                }}
            >
                <ArrowForwardIosIcon />
            </ButtonNext>
        </CarouselProvider>
    );
};

export default UsersCarousel;
