import React from 'react';
import { Avatar } from '@material-ui/core';

export const UsersMain = (props) => {
    const { users } = props;
    console.log(users);
    const renderUsers = users.map((userProfile, index) => {
        console.log(userProfile);
        return (
            <Avatar
                key={index}
                aria-label="recipe"
                alt="Profile Photo"
                src={userProfile.photos[0]}
            />
        );
    });
    return <div>{renderUsers}</div>;
};
