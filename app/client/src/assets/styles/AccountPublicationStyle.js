import { makeStyles } from '@material-ui/core/styles';
import instSpriteToggle from '../img/sprites/576406ccc24b.png';
import instSprite from '../img/sprites/c14ffe44a4f6.png';

const AccountPublicationStyle = makeStyles((theme) => ({
    instaPhotoWrapperColumn: {
        display: 'inline-block',
        maxWidth: '975px',
        marginBottom: '30px',
        marginRight: '30px',
        '&:nth-child(3n+3)': {
            marginRight: '0',
        },
    },
    instaPhotoItem: {
        width: '305px',
        height: '305px',
        position: 'relative',
    },
    instaPhotoItemImg: {
        width: '305px',
        height: '305px',
        // background: `url( ${instPhoto1})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    instaPhotoItemDisplayNoneBox: {
        position: 'absolute',
        width: '305px',
        height: '305px',
        backgroundColor: '#262626',
        opacity: '0',
        zIndex: '1000',
        '&:hover': {
            opacity: '0.5',
        },
    },
    instaPhotoItemFavorites: {},
    instaPhotoItemFavoritesIcon: {
        position: 'absolute',
        top: '148px',
        left: '100px',
        width: '19px',
        height: '17px',
        background: `url( ${instSpriteToggle})`,
        backgroundPosition: '-384px -126px',
        backgroundSize: 'initial',
    },
    instaPhotoItemFavoritesCount: {
        position: 'absolute',
        top: '145px',
        left: '125px',
        color: '#fafafa',
        fontWeight: '600',
    },
    instaPhotoItemComments: {},
    instaPhotoItemCommentsIcon: {
        position: 'absolute',
        top: '148px',
        left: '165px',
        width: '19px',
        height: '17px',
        background: `url( ${instSpriteToggle})`,
        backgroundPosition: '-384px -167px',
        backgroundSize: 'initial',
    },
    instaPhotoItemCommentsCount: {
        position: 'absolute',
        top: '145px',
        left: '190px',
        color: '#fafafa',
        fontWeight: '600',
    },
    dialogWrapper: {
        display: 'flex',
        flexDirection: 'row',
    },
    dialogPhotoWrapper: {
        width: '500px',
        height: '500px',
        overflow: 'hidden',
    },
    dialogPhotoItem: {
        width: '100%',
    },
    cardActionWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    instaFavoriteIconWrapper: {
        display: 'flex',
        height: '30px',
        width: '30px',
        margin: '0 auto',
        cursor: 'pointer',
        justifyContent: 'center',
        alignItems: 'center',
    },
    instaFavoriteIconFalse: {
        display: 'flex',
        width: '26px',
        height: '23px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-232px -370px',
        backgroundSize: 'initial',
    },
    instaFavoriteIconTrue: {
        display: 'flex',
        width: '26px',
        height: '23px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-206px -370px',
        backgroundSize: 'initial',
    },
    instaCommentIcon: {
        display: 'flex',
        width: '26px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-337px -344px',
        backgroundSize: 'initial',
    },
    instaSendMessage: {
        display: 'flex',
        width: '26px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '0 -368px',
        backgroundSize: 'initial',
    },

    instaSaveIconWrapper: {
        display: 'flex',
        height: '30px',
        width: '30px',
        margin: '0 auto',
        cursor: 'pointer',
        justifyContent: 'center',
        alignItems: 'center',
    },
    instaSaveIconFalse: {
        display: 'flex',
        width: '24px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-389px -396px',
        backgroundSize: 'initial',
    },
    instaSaveIconTrue: {
        display: 'flex',
        width: '24px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-311px -396px',
        backgroundSize: 'initial',
    },
    instaCommentFalse: {
        display: 'none',
    },
    instaCommentTrue: {
        display: 'flex',
    },
    dialogContentComments: {
        height: '190px',
    },
    postDate: {
        padding: 0,
        display: 'block',
    },
    postLikes: {
        padding: 0,
        display: 'block',
    },
}));

export default AccountPublicationStyle;
