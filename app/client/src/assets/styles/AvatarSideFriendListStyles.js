import { makeStyles } from '@material-ui/core/styles';

const AvatarSideFriendListStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    avatarName: {
        color: '#424242',
        fontWeight: '500',
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.fontSize,
    },
    avatarWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100px',
    },
}));

export default AvatarSideFriendListStyles;
