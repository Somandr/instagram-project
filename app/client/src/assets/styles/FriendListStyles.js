import { makeStyles } from '@material-ui/core/styles';

const FriendListStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        height: 190,
        maxWidth: 300,
        backgroundColor: theme.palette.background.paper,
    },
    subHeader: {
        color: '#757575',
        paddingTop: theme.spacing(1),
        paddingLeft: theme.spacing(1),
        fontWeight: '500',
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.fontSize,
    },
}));

export default FriendListStyles;
