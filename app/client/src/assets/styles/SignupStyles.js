import { makeStyles } from '@material-ui/core';
import instSprite from '../../assets/img/sprites/576406ccc24b.png';

const SignupStyles = makeStyles(() => ({
    signInSection: {
        height: '100vh',

        '& a': {
            textDecoration: 'none',
        },
        fontSize: '14px',
    },
    signInContainer: {
        height: '95%',
    },
    signInFormWrapper: {
        display: 'flex',
        flexDirection: 'column',
        width: '454px',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
    },

    instaLogoFormWrapper: {
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    instaLogoForm: {
        display: 'flex',
        width: '180px',
        height: '52px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-97px 0px',
        backgroundSize: 'initial',
        marginTop: '30px',
    },

    signInForm: {
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '350px',
        height: '100%',
        border: '1px solid #DADADA',
        marginBottom: '10px',
        marginTop: '12px',
    },
    inputsWrapper: {
        width: '268px',
        '& input': {
            marginBottom: '5px',
        },
        '& button': {
            margin: '10px 0 15px 0',
        },
    },
    titleSignUp: {
        display: 'inline-block',
        textAlign: 'center',
        color: '#8E8E8E',
        width: '100%',
        // marginBottom: '30px',
        position: 'relative',
        fontWeight: 'bold',
        fontSize: '16.8px',
    },
    devider: {
        display: 'inline-block',
        textAlign: 'center',
        color: '#8E8E8E',
        width: '100%',
        marginBottom: '30px',
        position: 'relative',
        fontWeight: 'bold',
        fontSize: '16.8px',
        '&:before': {
            content: "''",
            width: '100px',
            height: '1px',
            backgroundColor: '#B5B5B5',
            position: 'absolute',
            top: 10,
            left: 0,
        },
        '&:after': {
            content: "''",
            width: '100px',
            height: '1px',
            backgroundColor: '#B5B5B5',
            position: 'absolute',
            top: 10,
            right: 0,
        },
    },
    fbIconWrapper: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        textDecoration: 'none',
    },
    enterFacebook: {
        display: 'inline-block',
        fontWeight: 600,
        color: '#8E8E8E',
        textDecoration: 'none',
        fontSize: '12px',
        textAlign: 'center',
        paddingBottom: '45px',
    },
    fbIcon: {
        display: 'inline-block',
        width: '16px',
        height: '16px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-320px -243px',
        backgroundSize: 'initial',
        marginRight: '8px',
    },
    recoverPass: {
        display: 'inline-block',
        width: '100%',
        fontSize: '12px',
        color: '#00326F',
        marginBottom: '15px',
        textAlign: 'center',
    },
    signUp: {
        display: 'flex',
        width: '350px',
        height: '63px',
        fontSize: '14px',
        border: '1px solid #DADADA',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: '25px 0',
        color: '#262626',
        '& a': {
            color: '#0095f6',
            fontSize: '14px',
            fontWeight: 600,
            marginLeft: '5px',
            textDecoration: 'none',
        },
    },
    loadAdd: {
        width: '350px',
        textAlign: 'center',
        margin: '20px 0 20px 0',
    },
    adds: {
        display: 'flex',
        width: '350px',
        justifyContent: 'center',
    },
    appstoreAdd: {
        marginRight: '7px',
        '& img': {
            width: '140px',
        },
    },
    androidAdd: {
        '& img': {
            width: '140px',
        },
    },
    footer: {
        width: '1015px',
        margin: '0 auto',
        paddingTop: '110px',
        '& a': {
            textDecoration: 'none',
        },
    },
    footerLinks: {
        display: 'flex',
        marginRight: '16px',
        justifyContent: 'center',
        textTransform: 'uppercase',
        color: '#233A69',
        fontSize: '12px',
        fontWeight: 'bold',
        '& a': {
            marginRight: '16px',
            color: '#233A69',
        },
        '& a:visited': {
            color: '#233A69',
        },
    },
    footerCopyright: {
        display: 'inline-block',
        color: '#8E8E8E',
        fontSize: '12px',
        textTransform: 'uppercase',
        fontWeight: 700,
        margin: '15px 0 38px 0',
    },
    '@media screen and (max-width: 910px)': {
        titleSignUp: {
            display: 'none',
        },
        signInFormWrapper: {
            display: 'flex',
            flexDirection: 'column',
            width: '454px',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
        },
    },
}));

export default SignupStyles;
