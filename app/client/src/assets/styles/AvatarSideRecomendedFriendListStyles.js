import { makeStyles } from '@material-ui/core/styles';

const AvatarSideRecomendedFriendListStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
        marginRight: '7px',
    },
    avatarName: {
        color: '#262626',
        fontFamily: theme.typography.fontFamily,
        textTransform: 'lowercase',
        fontSize: '14px',
        fontWeight: 500,
    },
    avatarWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '150px',
    },
    subscribeWrapper: {
        textAlign: 'right',
    },
    subscribeText: {
        display: 'flex',
        alignItems: 'center',
        color: '#0095F6',
        fontWeight: 600,
        fontFamily: theme.typography.fontFamily,
        fontSize: '12px',
        textDecoration: 'none',
    },
}));

export default AvatarSideRecomendedFriendListStyles;
