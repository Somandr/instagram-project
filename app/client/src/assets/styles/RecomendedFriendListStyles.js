import { makeStyles } from '@material-ui/core/styles';

const RecomendedFriendListStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        height: 374,
        maxWidth: 400,
        backgroundColor: '##FAFAFA',
        paddingLeft: 30,
    },
    subHeader: {
        color: '#757575',
        // paddingTop: theme.spacing(1),
        paddingLeft: theme.spacing(1),
        fontWeight: 500,
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.fontSize,
    },
    footer: {
        width: '350px',
    },
    footerUl: {
        margin: '3px 0',
        paddingLeft: '0',
        width: '300px',
        listStyle: 'none',
    },
    footerLi: {
        textTransform: 'none',
        fontSize: '11px',
        fontWeight: '500',
        listStyleType: 'none',
        display: 'inline-block',
        paddingRight: '6px',
    },

    footerLinks: {
        textDecoration: 'none',
        color: '#757575',
    },

    footerCopyright: {
        textTransform: 'uppercase',
        fontSize: '11px',
        color: '#757575',
        fontFamily: theme.typography.fontFamily,
    },
}));

export default RecomendedFriendListStyles;
