import { makeStyles } from '@material-ui/core';

const NavBarStyles = makeStyles({
    logoWrap: {
        marginTop: 11,
        minWidth: 40,
    },
    searchWrap: {
        width: 'calc(23% - .5px)',
        paddingLeft: 106,
        paddingTop: 2,
    },
    inputSearch: { position: 'relative' },
    popper: {
        position: 'absolute',
        marginTop: 0,
        left: 493,
        '& a': {
            textDecoration: 'none',
        },
        '& a:visited': {
            color: 'black',
        },
    },
    iconsAppBar: {
        marginRight: 22,
    },
    linksAppBarWrap: {
        display: 'flex',
        justifyContant: 'space-between',
        alignItems: 'space-between',
        width: '20.5%',
        // flexDirection: 'row',
        paddingLeft: 18,
        marginTop: 5,
        // whiteSpace: 'nowrap',
    },
    activeLink: {
        color: '#FAFAFA',
    },
    '@media screen and (max-width: 910px)': {
        searchWrap: {
            display: 'none',
        },
    },
    '@media screen and (max-width: 780px)': {
        linksAppBarWrap: {
            display: 'flex',
            justifyContant: 'space-between',
            alignItems: 'space-between',
            width: '90%',
            // flexDirection: 'row',
            paddingLeft: 18,
            marginTop: 5,
            marginLeft: 60,
            // whiteSpace: 'nowrap',
        },
    },
});

export default NavBarStyles;
