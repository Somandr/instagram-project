const { Router } = require('express');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const sgTransport = require('nodemailer-sendgrid-transport');
const config = require('config');
const User = require('../models/User');

const router = Router();
const transporter = nodemailer.createTransport(
    sgTransport({
        auth: {
            api_key: config.get('SENDGRID_KEY'),
        },
    })
);

// =======================> /api/authorize/login
router.post(
    '/login',
    [
        check('email', 'Введите корректный email').normalizeEmail().isEmail(),
        check('password', 'Введите пароль').exists(),
    ],
    async (req, resp) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return resp.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при входе в систему',
                });
            }

            const { email, password } = req.body;

            const user = await User.findOne({ email });

            if (!user) {
                return resp
                    .status(400)
                    .json({ message: 'Пользователь не найден' });
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (!isMatch) {
                return resp
                    .status(400)
                    .json({ message: 'Неверный пароль, попробуйте снова' });
            }

            const token = jwt.sign(
                { userId: user.id },
                config.get('jwtSecret')
                // { expiresIn: '1h' },
            );

            resp.json({ token, userId: user.id, user });
        } catch (e) {
            resp.status(500).json({
                message: 'Что-то пошло не так, попробуйте снова',
            });
        }
    }
);

// =======================> /api/authorize/signup
router.post(
    '/signup',
    [
        check('email', 'Некорректный email').isEmail(),
        check('password', 'Необходимо ввести минимум 6 символов').isLength({
            min: 6,
        }),
    ],
    async (req, resp) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return resp.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при регистрации',
                });
            }

            const { email, name, username, password } = req.body;
            const challenger = await User.findOne({ email });

            if (challenger) {
                return resp.status(400).json({
                    message: 'Пользователь, с такими данными, уже существует!',
                });
            }

            const hashedPassword = await bcrypt.hash(password, 12);
            const user = new User({
                email,
                name,
                username,
                password: hashedPassword,
            });

            await user.save();
            transporter.sendMail({
                to: email,
                from: 'instagram.project.it@gmail.com',
                subject: 'Instagram Project - Account created successfully!',
                html:
                    '<h1>Welcome, now you are the member of Instagram Project!</h1>',
            });

            resp.status(201).json({ message: 'Пользователь создан, успешно!' });
        } catch (e) {
            resp.status(500).json({
                message: 'Что-то пошло не так :( пожалуйста, попробуйте снова!',
            });
        }
    }
);

// =======================> /api/authorize/password-recovery
router.post('/password-recovery', (req, res) => {
    crypto.randomBytes(32, (err, buffer) => {
        if (err) {
            // eslint-disable-next-line no-console
            console.log(err);
        }
        const token = buffer.toString('hex');
        User.findOne({ email: req.body.email }).then((user) => {
            if (!user) {
                return res
                    .status(422)
                    .json({ error: 'User does not exist with this email :(' });
            }
            user.resetToken = token;
            user.expireToken = Date.now() + 3600000;
            user.save().then((result) => {
                transporter.sendMail({
                    to: user.email,
                    from: 'instagram.project.it@gmail.com',
                    subject: 'Password reset',
                    html: `<h2>Have you requested a password change?</h2>
                    <h3>Please <a href='http://localhost:3000/password/${token}'>follow this link</a> to reset your password.</h3>`,
                });
                res.json({ message: 'check your eamil' });
            });
        });
    });
});

// =======================> /api/authorize/password-new
router.post('/password-new', (req, res) => {
    const { password, token } = req.body;

    User.findOne({
        resetToken: token,
        expireToken: { $gt: Date.now() },
    }).then((user) => {
        if (!user) {
            return res.status(422).json({ error: 'Session expired' });
        }

        bcrypt.hash(password, 12).then((hashedPassword) => {
            user.expireToken = undefined;
            user.password = hashedPassword;
            user.resetToken = undefined;
            user.save().then((result) => {
                res.json({
                    result,
                    message: 'Password updated successfully!',
                });
            });
        });
    });
});

module.exports = router;
