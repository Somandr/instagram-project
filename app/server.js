const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const config = require('config');
const mongoDb = require('./mongoDb/mongoDb');

const app = express();

app.use(express.json({ extended: true }));
app.disable('x-powered-by');
app.use(
    bodyParser.json({
        limit: '50mb',
    })
);

app.use(
    bodyParser.urlencoded({
        limit: '50mb',
        parameterLimit: 100000,
        extended: true,
    })
);
app.enable('trust proxy');

app.use('/api/authorize', require('./routes/auth.routes'));
app.use('/api/authorized/user', require('./routes/user.routes'));
app.use('/api/authorized/post', require('./routes/post.routes'));

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.join(__dirname, 'client', 'build')));

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const PORT = config.get('port') || 5555;

async function start() {
    try {
        mongoDb();
        app.listen(PORT, () =>
            // eslint-disable-next-line no-console
            console.log(`Server runing ==> location: ${PORT}`)
        );
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log('Server Error', e.message);
        process.exit(1);
    }
}

start();
