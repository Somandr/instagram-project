const AWS = require('aws-sdk');
const config = require('config');

const s3Client = new AWS.S3({
    accessKeyId: config.get('AWS_ACCESS_KEY'),
    secretAccessKey: config.get('AWS_SECRET_ACCESS_KEY'),
    region: config.get('REGION'),
});

const uploadParams = {
    Bucket: config.get('BUCKET'),
    Key: '',
    Body: null,
};

const s3 = {};
s3.s3Client = s3Client;
s3.uploadParams = uploadParams;

module.exports = s3;
